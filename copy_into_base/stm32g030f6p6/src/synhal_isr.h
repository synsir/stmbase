#pragma once
#include <synhal.h>

// set describes which one of the edges was triggered
// 1 for falling
// 2 for rising
// 3 for both
inline void syn_exti_0_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_1_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_2_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_3_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_4_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_5_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_6_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_7_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_8_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_9_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_10_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_11_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_12_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_13_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_14_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_exti_15_isr(uint16_t set)
{
  (void)(set);  // avoid "unsued variable" compiler warning
}

inline void syn_timer_1_isr()
{
}

inline void syn_timer_2_isr()
{
}

inline void syn_timer_3_isr()
{
}

inline void syn_timer_4_isr()
{
}

inline void syn_timer_5_isr()
{
}

inline void syn_dma_0_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_1_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_2_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_3_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_4_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_5_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_6_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_7_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_8_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_9_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_10_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_11_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_12_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_13_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_14_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}

inline void syn_dma_15_isr(uint16_t irq_status)
{
  (void)(irq_status);  // avoid "unsued variable" compiler warning
}